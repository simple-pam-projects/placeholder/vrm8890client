package scot.dagda.vrm8890Client;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.myspace.as.ApprovalGroup;
import com.myspace.as.ApprovalList;
import com.myspace.as.MultiFactor;
import com.myspace.as.RowData;

import org.drools.core.command.runtime.process.StartProcessCommand;
import org.drools.core.command.runtime.rule.FireAllRulesCommand;
import org.drools.core.command.runtime.rule.InsertObjectCommand;
import org.kie.api.KieServices;
import org.kie.api.command.BatchExecutionCommand;
import org.kie.api.command.Command;
import org.kie.api.command.KieCommands;
import org.kie.api.runtime.ExecutionResults;
import org.kie.server.api.marshalling.Marshaller;
import org.kie.server.api.marshalling.MarshallerFactory;
import org.kie.server.api.marshalling.MarshallingFormat;
import org.kie.server.api.model.KieServiceResponse.ResponseType;
import org.kie.server.api.model.ServiceResponse;
import org.kie.server.client.DMNServicesClient;
import org.kie.server.client.KieServicesClient;
import org.kie.server.client.KieServicesConfiguration;
import org.kie.server.client.KieServicesFactory;
import org.kie.server.client.RuleServicesClient;

import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;
import org.drools.core.base.RuleNameEqualsAgendaFilter;
import org.kie.api.runtime.rule.QueryResults;
import org.kie.api.runtime.rule.QueryResultsRow;
import org.kie.dmn.api.core.DMNContext;
import org.kie.dmn.api.core.DMNDecisionResult;
import org.kie.dmn.api.core.DMNMessage;
import org.kie.dmn.api.core.DMNResult;

import scot.dagda.model.MflDirect;
import scot.dagda.model.MflDirectWeights;
import scot.dagda.vrm8890DataModel.Audit;
import scot.dagda.vrm8890DataModel.Lease;
import scot.dagda.vrm8890DataModel.SalesRecommendation;
import scot.dagda.vrm8890DataModel.Vehicle;

/**
 * Unit test for simple App.
 */
public class AppTest extends TestCase {

    private final String QUERY_SALES_RECOMMENDATION = "SALES_RECOMMENDATION_QUERY";
    private final String QUERY_SALES_RECOMMENDATION_RULES_NAME = "GET_SalesRecommendation";
    //
    private final String QUERY_RESPONSE = "QUERY_RESPONSE";
    private final String QUERY_RESPONSE_RULES_NAME = "queryresponse";
    //
    private final String QUERY_AUDIT = "AUDIT_QUERY";
    private final String QUERY_AUDIT_RULES_NAME = "GET_Audit";
    //
    private String fancyPrefix = "[" + this.getClass().getPackage().getName() + "] ..::|| ";

    /**
     * Create the test case
     *
     * @param testName name of the test case
     */
    public AppTest(String testName) {
        super(testName);
    }

    /**
     * @return the suite of tests being tested
     */
    public static Test suite() {
        return new TestSuite(AppTest.class);
    }

    public void callDMN(KieServicesClient client, String dmn_container, String dmn_modelNameSpace, String dmn_modelName,
            HashMap<String, Object> dmnData) {

        DMNServicesClient dmnClient = client.getServicesClient(DMNServicesClient.class);
        DMNContext dmnContext = dmnClient.newContext();

        dmnData.keySet().forEach(key -> {
            dmnContext.set(key, dmnData.get(key));
        });

        ServiceResponse<DMNResult> serverResp = dmnClient.evaluateAll(dmn_container, dmn_modelNameSpace, dmn_modelName,
                dmnContext);
        DMNResult dmnResult = serverResp.getResult();

        for (DMNDecisionResult dr : dmnResult.getDecisionResults()) {
            System.out.println(
                    fancyPrefix + "Decision: '" + dr.getDecisionName() + "', " + dr.getEvaluationStatus() + ", "
                            + "Result: " + dr.getResult());
            for (DMNMessage msg : dr.getMessages()) {
                System.out.println(fancyPrefix + "Decision Messages: '" + msg);
            }
        }

    }

    public void createDataForAgFlow(Map<String, Object> bpmnDataMap) {
        ObjectMapper omap = new ObjectMapper();

        ArrayList<RowData> rowList = new ArrayList<>();
        {
            ApprovalGroup ag1 = new ApprovalGroup(1, "GroupA1");
            ApprovalGroup ag2 = new ApprovalGroup(2, "GroupB1");

            ApprovalList approvalList = new ApprovalList();
            approvalList.addApprovalGroup(ag1);
            approvalList.addApprovalGroup(ag2);

            RowData rd = new RowData(1, 0, 10);
            rd.addApprovalList(approvalList);

            rowList.add(rd);
        }
        {
            ApprovalGroup ag1 = new ApprovalGroup(1, "GroupA2");
            ApprovalGroup ag2 = new ApprovalGroup(2, "GroupB2");

            ApprovalList approvalList = new ApprovalList();
            approvalList.addApprovalGroup(ag1);
            approvalList.addApprovalGroup(ag2);

            RowData rd = new RowData(2, 10, 50);
            rd.addApprovalList(approvalList);

            rowList.add(rd);
        }
        {
            RowData rd = new RowData(3, 50, 100);
            {
                ApprovalGroup ag1 = new ApprovalGroup(1, "GroupA3");
                ApprovalGroup ag2 = new ApprovalGroup(2, "GroupB3");

                ApprovalList approvalList = new ApprovalList();
                approvalList.addApprovalGroup(ag1);
                approvalList.addApprovalGroup(ag2);
                rd.addApprovalList(approvalList);
            }
            {
                ApprovalGroup ag1 = new ApprovalGroup(1, "GroupC3");

                ApprovalList approvalList = new ApprovalList();
                approvalList.addApprovalGroup(ag1);
                rd.addApprovalList(approvalList);
            }

            rowList.add(rd);
        }
        bpmnDataMap.put("RowData", omap.convertValue(rowList, List.class));

        bpmnDataMap.put("RequestNumber", 60);
    }

    /**
     * Rigorous Test :-)
     */
    public void testApp() {

        String url = "http://localhost:8080/kie-server/services/rest/server";
        String username = "kieServerUser";
        String password = "kieServerUser1234;";

        String container = "mo_vrm";
        String session = "restLess";

        boolean invokeDMN = true;
        boolean invokeDRL = false;
        boolean invokeRuleFlow = true;
        boolean invokeMo = false;
        String dmn_container = "mo_vrm";

        String dmn_modelNameSpace = "https://kiegroup.org/dmn/_5284B315-C526-400B-8CC9-4360351CCFFB";
        // String dmn_modelName = "PreExpiry2";
        String dmn_modelName = "PreExpiry_MoreData";

        // String dmn_modelNameSpace =
        // "https://kiegroup.org/dmn/_16A8BA90-F0B6-4A6A-9643-9C84BCFB1034";
        // String dmn_modelName = "PreExpiry";
        KieServicesConfiguration config = KieServicesFactory.newRestConfiguration(url, username, password);

        config.setMarshallingFormat(MarshallingFormat.JSON);

        Set<Class<?>> allClasses = new HashSet<Class<?>>();
        allClasses.add(SalesRecommendation.class);
        allClasses.add(Audit.class);
        allClasses.add(Vehicle.class);
        allClasses.add(com.test.rule_flows.Vehicle.class);
        allClasses.add(Lease.class);
        config.addExtraClasses(allClasses);

        KieServicesClient client = KieServicesFactory.newKieServicesClient(config);

        RuleServicesClient ruleClient = client.getServicesClient(RuleServicesClient.class);

        KieCommands kieCommander = KieServices.Factory.get().getCommands();

        if (invokeRuleFlow) {

            //
            // -- invoke "recommendation" ruleflow
            //
            {
                container = "tmns";
                ObjectMapper omap = new ObjectMapper();

                Map<String, Object> ruleFlowMap = new HashMap<>();
                com.test.rule_flows.Vehicle v = new com.test.rule_flows.Vehicle();
                v.setHasUnresolvedItem(true);

                ruleFlowMap.put("Vehicle", omap.convertValue(v, com.test.rule_flows.Vehicle.class));

                List<Command<?>> commands = new ArrayList<Command<?>>();
                for (Command<?> c:commands) {

                }
                commands.add(new StartProcessCommand("rule_flows.recommendation", ruleFlowMap));
                BatchExecutionCommand batchCommand = kieCommander.newBatchExecution(commands, "myStatelessSession");
                try {
                    Marshaller marshallerJSON = MarshallerFactory.getMarshaller(allClasses, MarshallingFormat.JSON,
                            getClass().getClassLoader());
                    String bbJSON = marshallerJSON.marshall(batchCommand);
                    FileWriter fw = new FileWriter(new File(("ruleFlowRequest.json")));
                    fw.write(bbJSON);
                    fw.close();
                    System.out.println(fancyPrefix + "batchCommand has been saved as JSON to ruleFlowRequest.json");
                } catch (Exception e) {
                    e.printStackTrace();
                }
                ServiceResponse<ExecutionResults> response = ruleClient.executeCommandsWithResults(container,
                        batchCommand);
            }

            //
            // -- invoke "ag_flow" BPMN model
            //
            {
                container = "tmns";
                ObjectMapper omap = new ObjectMapper();

                Map<String, Object> ruleFlowMap = new HashMap<>();

                createDataForAgFlow(ruleFlowMap);

                List<Command<?>> commands = new ArrayList<Command<?>>();
                commands.add(new StartProcessCommand("morf2.ag_flow", ruleFlowMap));
                BatchExecutionCommand batchCommand = kieCommander.newBatchExecution(commands, "myStatelessSession");
                try {
                    Marshaller marshallerJSON = MarshallerFactory.getMarshaller(allClasses, MarshallingFormat.JSON,
                            getClass().getClassLoader());
                    String bbJSON = marshallerJSON.marshall(batchCommand);
                    FileWriter fw = new FileWriter(new File(("ag_flow.json")));
                    fw.write(bbJSON);
                    fw.close();
                    System.out.println(fancyPrefix + "batchCommand has been saved as JSON to ruleFlowRequest.json");
                } catch (Exception e) {
                    e.printStackTrace();
                }
                ServiceResponse<ExecutionResults> response = ruleClient.executeCommandsWithResults(container,
                        batchCommand);
            }

        }
        //
        // --- Invoke DRL ---
        //
        if (invokeDRL) {

            List<Command<?>> commands = new ArrayList<Command<?>>();
            {
                Vehicle fact = new Vehicle();
                fact.setVehicleId("v1car");
                // fact.setVehicleType("CAR");
                commands.add(new InsertObjectCommand(fact));
            }
            {
                Vehicle fact = new Vehicle();
                fact.setVehicleId("v2wav");
                // fact.setVehicleType("WAV");
                commands.add(new InsertObjectCommand(fact));
            }
            {
                Vehicle fact = new Vehicle();
                fact.setVehicleId("v2lcv");
                // fact.setVehicleType("LCV");
                commands.add(new InsertObjectCommand(fact));
            }
            {
                Vehicle fact = new Vehicle();
                fact.setVehicleId("v1car_30");
                // fact.setVehicleType("CAR");
                commands.add(new InsertObjectCommand(fact));
            }
            {
                Lease fact = new Lease();
                fact.setVehicleId("v1car_30");
                fact.setExpiryDate(LocalDate.of(2020, 9, 01));
                commands.add(new InsertObjectCommand(fact));
            }

            // commands.add(new FireAllRulesCommand("fireAll"));
            RuleNameEqualsAgendaFilter agendaFilter = new RuleNameEqualsAgendaFilter("SPECIFIC_RULE");
            commands.add(new FireAllRulesCommand("fireAll", -1, agendaFilter));

            commands.add((Command<?>) kieCommander.newQuery(QUERY_AUDIT, QUERY_AUDIT_RULES_NAME));
            commands
                    .add((Command<?>) kieCommander.newQuery(QUERY_SALES_RECOMMENDATION,
                            QUERY_SALES_RECOMMENDATION_RULES_NAME));
            commands.add((Command<?>) kieCommander.newQuery(QUERY_RESPONSE, QUERY_RESPONSE_RULES_NAME));

            BatchExecutionCommand batchCommand = kieCommander.newBatchExecution(commands, session);
            {
                try {
                    Marshaller marshallerJAXB = MarshallerFactory.getMarshaller(allClasses, MarshallingFormat.JAXB,
                            this.getClass().getClassLoader());
                    String bbXML = marshallerJAXB.marshall(batchCommand);
                    FileWriter fw = new FileWriter(new File(("batchCommand.xml")));
                    fw.write(bbXML);
                    fw.close();
                    System.out.println(fancyPrefix + "batchCommand has been saved as XML to batchCommand.xml");
                } catch (IOException e) {
                    e.printStackTrace();
                }

                try {
                    Marshaller marshallerJSON = MarshallerFactory.getMarshaller(allClasses, MarshallingFormat.JSON,
                            getClass().getClassLoader());
                    String bbJSON = marshallerJSON.marshall(batchCommand);
                    FileWriter fw = new FileWriter(new File(("batchCommand.json")));
                    fw.write(bbJSON);
                    fw.close();
                    System.out.println(fancyPrefix + "batchCommand has been saved as JSON to batchCommand.json");
                } catch (Exception e) {
                    e.printStackTrace();
                }

                try {
                    Marshaller marshallerXSTREAM = MarshallerFactory.getMarshaller(allClasses,
                            MarshallingFormat.XSTREAM,
                            this.getClass().getClassLoader());
                    String bbXSTREAM = marshallerXSTREAM.marshall(batchCommand);
                    FileWriter fw = new FileWriter(new File(("batchCommand.xstream")));
                    fw.write(bbXSTREAM);
                    fw.close();
                    System.out.println(fancyPrefix + "batchCommand has been saved as XSTREAM to batchCommand.xstream");
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            ServiceResponse<ExecutionResults> response = ruleClient.executeCommandsWithResults(container, batchCommand);

            if (response.getType().compareTo(ResponseType.SUCCESS) == 0) {
                System.out.println(fancyPrefix);
                System.out.println(fancyPrefix + "SUCCESS : " + container + " has been invoked");
                System.out.println(fancyPrefix);
            }

            System.out.println(fancyPrefix + "response.getMsg(): " + response.getMsg() + " ||::..");
            System.out.println(fancyPrefix + "response.getResult(): " + response.getResult() + " ||::..");

            if (response.getResult() != null) {
                QueryResults qResults = (QueryResults) response.getResult().getValue(QUERY_AUDIT);
                System.out.println(fancyPrefix + "DRL Query Results : Audit Size : " + qResults.size());
                for (QueryResultsRow query : qResults) {
                    Audit audit = (Audit) query.get("q");
                    System.out.println(fancyPrefix + "DRL Query Results : Audit : " + audit);
                }
                System.out.println(fancyPrefix);
            }

            if (response.getResult() != null) {
                QueryResults qResults = (QueryResults) response.getResult().getValue(QUERY_SALES_RECOMMENDATION);
                System.out.println(fancyPrefix + "DRL Query Results : SalesRecommendation Size : " + qResults.size());
                for (QueryResultsRow query : qResults) {
                    SalesRecommendation sales = (SalesRecommendation) query.get("q");
                    System.out.println(fancyPrefix + "DRL Query Results : SalesRecommendation : " + sales);
                }
                System.out.println(fancyPrefix);
            }

            if (response.getResult() != null) {
                QueryResults qResults = (QueryResults) response.getResult().getValue(QUERY_RESPONSE);
                System.out.println(
                        fancyPrefix + QUERY_RESPONSE + " :: DRL Query Results : SalesRecommendation Size : "
                                + qResults.size());
                for (QueryResultsRow query : qResults) {
                    SalesRecommendation sales = (SalesRecommendation) query.get("response");
                    System.out.println(
                            fancyPrefix + QUERY_RESPONSE + " :: DRL Query Results : SalesRecommendation : " + sales);
                }
                System.out.println(fancyPrefix);
            }
        }

        //
        // --- Invoke DMN ---
        //
        if (invokeDMN) {

            System.out.println(fancyPrefix);
            System.out.println(fancyPrefix + "INVOKING DMN");
            System.out.println(fancyPrefix);

            // {
            // dmn_container = "as";
            // dmn_modelName = "rank-03-multi";
            // dmn_modelNameSpace =
            // "https://kiegroup.org/dmn/_3F87C51F-2674-4C73-A129-0FF39131D59B";
            //
            // HashMap<String, Object> dmnData = new HashMap<>();
            // dmnData.put("IndicatorA", "yes");
            // dmnData.put("IndicatorB", "yes");
            // dmnData.put("IndicatorC", "no");
            // dmnData.put("IndicatorD", "no");
            // dmnData.put("IndicatorE", "no");
            // dmnData.put("CanRefurb", "yes");
            // dmnData.put("PostRefurb", "no");
            // callDMN(client, dmn_container, dmn_modelNameSpace, dmn_modelName, dmnData);
            // }
            if (invokeMo) {
                {
                    dmn_container = "tmns";
                    dmn_modelName = "mflPriceEstimate";
                    dmn_modelNameSpace = "https://kiegroup.org/dmn/_A596C8EA-A4A3-4EF4-80C9-0C7B7AD0388C";

                    HashMap<String, Object> dmnData = new HashMap<>();

                    dmnData.put("mflPrice", 10000);
                    dmnData.put("mflDaysToSell", 2);
                    dmnData.put("mflDepreciation", 0.2);
                    dmnData.put("mflSeasonality", 100);

                    dmnData.put("mflPriceW", 1);
                    dmnData.put("mflDepreciationW", -1);
                    dmnData.put("mflSeasonalityW", -1);

                    callDMN(client, dmn_container, dmn_modelNameSpace, dmn_modelName, dmnData);
                }

                {
                    dmn_container = "tmns";
                    dmn_modelName = "rank-02t";
                    dmn_modelNameSpace = "https://kiegroup.org/dmn/_3F87C51F-2674-4C73-A129-0FF39131D59B";

                    ObjectMapper omap = new ObjectMapper();

                    HashMap<String, Object> dmnData = new HashMap<>();
                    dmnData.put("FactorA", 2);
                    dmnData.put("FactorB", 3);
                    dmnData.put("FactorC", 4);

                    MultiFactor mfo = new MultiFactor();
                    mfo.setFa(1);
                    mfo.setFb(2);
                    mfo.setFc(3);
                    dmnData.put("multiFactor", omap.convertValue(mfo, Map.class));

                    callDMN(client, dmn_container, dmn_modelNameSpace, dmn_modelName, dmnData);
                }
            }

            {
                dmn_container = "tmns";
                dmn_modelName = "ar_group_selection";
                dmn_modelNameSpace = "https://kiegroup.org/dmn/_E19245A7-5775-469D-9A0F-8F81210F1F43";

                ObjectMapper omap = new ObjectMapper();

                HashMap<String, Object> dmnData = new HashMap<>();

                ArrayList<RowData> rowList = new ArrayList<>();
                {
                    ApprovalGroup ag1 = new ApprovalGroup(1, "GroupA1");
                    ApprovalGroup ag2 = new ApprovalGroup(2, "GroupB1");

                    ApprovalList approvalList = new ApprovalList();
                    approvalList.addApprovalGroup(ag1);
                    approvalList.addApprovalGroup(ag2);

                    RowData rd = new RowData(1, 0, 10);
                    rd.addApprovalList(approvalList);

                    rowList.add(rd);
                }
                {
                    ApprovalGroup ag1 = new ApprovalGroup(1, "GroupA2");
                    ApprovalGroup ag2 = new ApprovalGroup(2, "GroupB2");

                    ApprovalList approvalList = new ApprovalList();
                    approvalList.addApprovalGroup(ag1);
                    approvalList.addApprovalGroup(ag2);

                    RowData rd = new RowData(2, 10, 50);
                    rd.addApprovalList(approvalList);

                    rowList.add(rd);
                }
                {
                    RowData rd = new RowData(3, 50, 100);
                    {
                        ApprovalGroup ag1 = new ApprovalGroup(1, "GroupA3");
                        ApprovalGroup ag2 = new ApprovalGroup(2, "GroupB3");

                        ApprovalList approvalList = new ApprovalList();
                        approvalList.addApprovalGroup(ag1);
                        approvalList.addApprovalGroup(ag2);
                        rd.addApprovalList(approvalList);
                    }
                    {
                        ApprovalGroup ag1 = new ApprovalGroup(1, "GroupC3");

                        ApprovalList approvalList = new ApprovalList();
                        approvalList.addApprovalGroup(ag1);
                        rd.addApprovalList(approvalList);
                    }

                    rowList.add(rd);
                }
                dmnData.put("RowData", omap.convertValue(rowList, List.class));

                dmnData.put("RequestNumber", 60);

                callDMN(client, dmn_container, dmn_modelNameSpace, dmn_modelName, dmnData);
            }

            //
            // -- OLD
            //
            // {
            // LocalDate startDate = LocalDate.now();
            // LocalDate endDate = LocalDate.of(2020, 9, 01);
            // long daysBetween = ChronoUnit.DAYS.between(endDate, startDate);
            //
            // HashMap<String, Object> dmnData = new HashMap<>();
            // dmnData.put("Vehicle Type", "CAR");
            // dmnData.put("Lease Expiry Date", daysBetween);
            // dmnData.put("Vehicle Condition", "good");
            //
            // callDMN(client, dmn_container, dmn_modelNameSpace, dmn_modelName, dmnData);
            // }
            //
            // {
            // LocalDate startDate = LocalDate.now();
            // LocalDate endDate = LocalDate.of(2020, 9, 01);
            // long daysBetween = ChronoUnit.DAYS.between(endDate, startDate);
            //
            // HashMap<String, Object> dmnData = new HashMap<>();
            // dmnData.put("Vehicle Type", "CAR");
            // dmnData.put("Lease Expiry Date", daysBetween);
            // dmnData.put("Vehicle Condition", "bad");
            //
            // callDMN(client, dmn_container, dmn_modelNameSpace, dmn_modelName, dmnData);
            // }
            //
            // {
            // LocalDate startDate = LocalDate.now();
            // LocalDate endDate = LocalDate.of(2020, 9, 01);
            // long daysBetween = ChronoUnit.DAYS.between(endDate, startDate);
            //
            // HashMap<String, Object> dmnData = new HashMap<>();
            // dmnData.put("Vehicle Type", "WAV");
            // dmnData.put("Lease Expiry Date", 0);
            // dmnData.put("Vehicle Condition", "");
            //
            // callDMN(client, dmn_container, dmn_modelNameSpace, dmn_modelName, dmnData);
            // }
            //
            // {
            // Vehicle v = new Vehicle();
            // v.setVehicleType("CAR");
            // v.setCondition("good");
            //
            // HashMap<String, Object> dmnData = new HashMap<>();
            // // dmnData.put("Vehicle Type", "CAR");
            // // dmnData.put("Vehicle Condition", "good");
            // dmnData.put("Vehicle", v);
            //
            // // callDMN(client, dmn_container, dmn_modelNameSpace, dmn_modelName,
            // dmnData);
            // }
        }

        assertTrue(true);
    }

}
