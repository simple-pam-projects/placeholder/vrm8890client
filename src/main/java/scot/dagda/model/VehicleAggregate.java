package scot.dagda.model;

public class VehicleAggregate {

    Double numberOfVehicles;
    String vehicleType;


    public VehicleAggregate() {
    }

    public VehicleAggregate(Double numberOfVehicles, String vehicleType) {
        this.numberOfVehicles = numberOfVehicles;
        this.vehicleType = vehicleType;
    }

    public Double getNumberOfVehicles() {
        return this.numberOfVehicles;
    }

    public void setNumberOfVehicles(Double numberOfVehicles) {
        this.numberOfVehicles = numberOfVehicles;
    }

    public String getVehicleType() {
        return this.vehicleType;
    }

    public void setVehicleType(String vehicleType) {
        this.vehicleType = vehicleType;
    }

    public VehicleAggregate numberOfVehicles(Double numberOfVehicles) {
        setNumberOfVehicles(numberOfVehicles);
        return this;
    }

    public VehicleAggregate vehicleType(String vehicleType) {
        setVehicleType(vehicleType);
        return this;
    }

    @Override
    public String toString() {
        return "{" +
            " numberOfVehicles='" + getNumberOfVehicles() + "'" +
            ", vehicleType='" + getVehicleType() + "'" +
            "}";
    }


}
