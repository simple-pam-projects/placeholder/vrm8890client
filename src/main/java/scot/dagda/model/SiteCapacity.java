package scot.dagda.model;

public class SiteCapacity {
    
    String vehicleType;
    Double capacity;


    public SiteCapacity() {
    }

    public SiteCapacity(String vehicleType, Double capacity) {
        this.vehicleType = vehicleType;
        this.capacity = capacity;
    }

    public String getVehicleType() {
        return this.vehicleType;
    }

    public void setVehicleType(String vehicleType) {
        this.vehicleType = vehicleType;
    }

    public Double getCapacity() {
        return this.capacity;
    }

    public void setCapacity(Double capacity) {
        this.capacity = capacity;
    }

    public SiteCapacity vehicleType(String vehicleType) {
        setVehicleType(vehicleType);
        return this;
    }

    public SiteCapacity capacity(Double capacity) {
        setCapacity(capacity);
        return this;
    }

    @Override
    public String toString() {
        return "{" +
            " vehicleType='" + getVehicleType() + "'" +
            ", capacity='" + getCapacity() + "'" +
            "}";
    }


}
