package scot.dagda.model;

import java.math.BigDecimal;

public class MflDirectWeights {
    
    BigDecimal price;
    BigDecimal daysToSell;
    BigDecimal depreciation;
    BigDecimal seasonality;


    public MflDirectWeights() {
    }

    public MflDirectWeights(BigDecimal price, BigDecimal daysToSell, BigDecimal depreciation, BigDecimal seasonality) {
        this.price = price;
        this.daysToSell = daysToSell;
        this.depreciation = depreciation;
        this.seasonality = seasonality;
    }

    public BigDecimal getPrice() {
        return this.price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public BigDecimal getDaysToSell() {
        return this.daysToSell;
    }

    public void setDaysToSell(BigDecimal daysToSell) {
        this.daysToSell = daysToSell;
    }

    public BigDecimal getDepreciation() {
        return this.depreciation;
    }

    public void setDepreciation(BigDecimal depreciation) {
        this.depreciation = depreciation;
    }

    public BigDecimal getSeasonality() {
        return this.seasonality;
    }

    public void setSeasonality(BigDecimal seasonality) {
        this.seasonality = seasonality;
    }

    public MflDirectWeights price(BigDecimal price) {
        setPrice(price);
        return this;
    }

    public MflDirectWeights daysToSell(BigDecimal daysToSell) {
        setDaysToSell(daysToSell);
        return this;
    }

    public MflDirectWeights depreciation(BigDecimal depreciation) {
        setDepreciation(depreciation);
        return this;
    }

    public MflDirectWeights seasonality(BigDecimal seasonality) {
        setSeasonality(seasonality);
        return this;
    }

    @Override
    public String toString() {
        return "{" +
            " price='" + getPrice() + "'" +
            ", daysToSell='" + getDaysToSell() + "'" +
            ", depreciation='" + getDepreciation() + "'" +
            ", seasonality='" + getSeasonality() + "'" +
            "}";
    }

}
