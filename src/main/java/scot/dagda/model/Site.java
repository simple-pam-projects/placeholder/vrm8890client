package scot.dagda.model;

public class Site {

    Double distance;
    String overwater;
    Double movingCost;
    Double daysToNextEvent;


    public Site() {
    }

    public Site(Double distance, String overwater, Double movingCost, Double daysToNextEvent) {
        this.distance = distance;
        this.overwater = overwater;
        this.movingCost = movingCost;
        this.daysToNextEvent = daysToNextEvent;
    }

    public Double getDistance() {
        return this.distance;
    }

    public void setDistance(Double distance) {
        this.distance = distance;
    }

    public String getOverwater() {
        return this.overwater;
    }

    public void setOverwater(String overwater) {
        this.overwater = overwater;
    }

    public Double getMovingCost() {
        return this.movingCost;
    }

    public void setMovingCost(Double movingCost) {
        this.movingCost = movingCost;
    }

    public Double getDaysToNextEvent() {
        return this.daysToNextEvent;
    }

    public void setDaysToNextEvent(Double daysToNextEvent) {
        this.daysToNextEvent = daysToNextEvent;
    }

    public Site distance(Double distance) {
        setDistance(distance);
        return this;
    }

    public Site overwater(String overwater) {
        setOverwater(overwater);
        return this;
    }

    public Site movingCost(Double movingCost) {
        setMovingCost(movingCost);
        return this;
    }

    public Site daysToNextEvent(Double daysToNextEvent) {
        setDaysToNextEvent(daysToNextEvent);
        return this;
    }

    @Override
    public String toString() {
        return "{" +
            " distance='" + getDistance() + "'" +
            ", overwater='" + getOverwater() + "'" +
            ", movingCost='" + getMovingCost() + "'" +
            ", daysToNextEvent='" + getDaysToNextEvent() + "'" +
            "}";
    }

    
}
