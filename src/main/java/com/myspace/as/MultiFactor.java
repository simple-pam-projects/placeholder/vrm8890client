package com.myspace.as;

import java.io.Serializable;

public class MultiFactor implements Serializable {

    Integer fa;
    Integer fb;
    Integer fc;

    public MultiFactor() {
    }

    public MultiFactor(Integer fa, Integer fb, Integer fc) {
        this.fa = fa;
        this.fb = fb;
        this.fc = fc;
    }

    public Integer getFa() {
        return this.fa;
    }

    public void setFa(Integer fa) {
        this.fa = fa;
    }

    public Integer getFb() {
        return this.fb;
    }

    public void setFb(Integer fb) {
        this.fb = fb;
    }

    public Integer getFc() {
        return this.fc;
    }

    public void setFc(Integer fc) {
        this.fc = fc;
    }

    public MultiFactor fa(Integer fa) {
        setFa(fa);
        return this;
    }

    public MultiFactor fb(Integer fb) {
        setFb(fb);
        return this;
    }

    public MultiFactor fc(Integer fc) {
        setFc(fc);
        return this;
    }

    @Override
    public String toString() {
        return "{" + " fa='" + getFa() + "'" + ", fb='" + getFb() + "'" + ", fc='" + getFc() + "'" + "}";
    }

}
