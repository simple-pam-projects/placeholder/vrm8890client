package com.myspace.as;

import java.util.ArrayList;

public class RowData {
    Integer row_number;
    Integer low_limit;
    Integer upper_limit;
    ArrayList<ApprovalList> approvalList;

    public RowData() {
        approvalList = new ArrayList<>();
    }

    public RowData(Integer row_number, Integer low_limit, Integer upper_limit) {
        this.row_number = row_number;
        this.low_limit = low_limit;
        this.upper_limit = upper_limit;
        this.approvalList = new ArrayList<>();
    }

    public void addApprovalList(ApprovalList al) {
        if (getApprovalList() == null) {
            this.approvalList = new ArrayList<>();
        }
        this.approvalList.add(al);
    }

    public ArrayList<ApprovalList> getApprovalList() {
        return this.approvalList;
    }

    public void setApprovalList(ArrayList<ApprovalList> approvalList) {
        this.approvalList = approvalList;
    }

    public Integer getRow_number() {
        return this.row_number;
    }

    public void setRow_number(Integer row_number) {
        this.row_number = row_number;
    }

    public Integer getLow_limit() {
        return this.low_limit;
    }

    public void setLow_limit(Integer low_limit) {
        this.low_limit = low_limit;
    }

    public Integer getUpper_limit() {
        return this.upper_limit;
    }

    public void setUpper_limit(Integer upper_limit) {
        this.upper_limit = upper_limit;
    }

    public RowData row_number(Integer row_number) {
        setRow_number(row_number);
        return this;
    }

    public RowData low_limit(Integer low_limit) {
        setLow_limit(low_limit);
        return this;
    }

    public RowData upper_limit(Integer upper_limit) {
        setUpper_limit(upper_limit);
        return this;
    }

    @Override
    public String toString() {
        return "{" +
                " row_number='" + getRow_number() + "'" +
                ", low_limit='" + getLow_limit() + "'" +
                ", upper_limit='" + getUpper_limit() + "'" +
                "}";
    }

}
